public class Main {
    public static void main(String[] args) {

    }

    public static int [] remove(int number, int [] myArray) {

        int [] tempArr = new int[myArray.length - 1];
        int counter = 0;
        for (int value : myArray) {
            if (value == number) {
                continue;
            }
            tempArr[counter] = value;
            counter++;
        }
        myArray = tempArr;

        return myArray;
    }

    public static boolean add(int value, int [] testArr) {

        int[] tempArr = new int[testArr.length + 1];
        for (int i = 0; i < testArr.length; i++) {
            tempArr[i] = testArr[i];
        }
        tempArr [ tempArr.length - 1] = value;
        testArr = tempArr;
        return false;
    }

}


